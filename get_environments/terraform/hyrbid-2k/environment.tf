module "gitlab_ref_arch_gcp" {
  source = "../../modules/gitlab_ref_arch_gcp"

  prefix                  = var.prefix
  project                 = var.project
  object_storage_location = var.object_storage_location
  service_account_prefix  = var.service_account_prefix

  # k8s - todo tweak back
  #webservice_node_pool_max_count    = 3
  #webservice_node_pool_min_count    = 1
  #webservice_node_pool_machine_type = "n1-standard-32"

  #sidekiq_node_pool_max_count    = 4
  #sidekiq_node_pool_min_count    = 1
  #sidekiq_node_pool_machine_type = "n1-standard-4"

  #supporting_node_pool_max_count    = 2
  #supporting_node_pool_min_count    = 1
  #supporting_node_pool_machine_type = "n1-standard-4"
  # 1k
  gitlab_rails_node_count   = 3
  gitlab_rails_machine_type = "n1-standard-8"

  haproxy_external_node_count   = 1
  haproxy_external_machine_type = "n1-highcpu-2"
  haproxy_external_external_ips = [var.external_ip]
  
  # 3k
  consul_node_count   = 3
  consul_machine_type = "e2-highcpu-2"

  gitaly_node_count   = 3
  gitaly_machine_type = "e2-standard-2"

  praefect_node_count   = 3
  praefect_machine_type = "e2-highcpu-2"

  praefect_postgres_node_count   = 1
  praefect_postgres_machine_type = "e2-highcpu-2"

  haproxy_internal_node_count   = 1
  haproxy_internal_machine_type = "e2-highcpu-2"

  pgbouncer_node_count   = 3
  pgbouncer_machine_type = "e2-highcpu-2"

  postgres_node_count   = 3
  postgres_machine_type = "e2-standard-2"

  redis_node_count   = 3
  redis_machine_type = "e2-standard-2"

}

output "gitlab_ref_arch_gcp" {
  value = module.gitlab_ref_arch_gcp
}
